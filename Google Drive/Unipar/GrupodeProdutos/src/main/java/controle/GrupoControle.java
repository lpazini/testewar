/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidade.Grupo;
import facade.GrupoFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author DaviD W.a.R
 */
@ManagedBean
@SessionScoped
public class GrupoControle {
    
    private Grupo grupo;  
    @EJB
    private GrupoFacade grupoFacade;
    
    
    
    public void novo(){
        grupo = new Grupo();
    }
    
    public void salvar(){
        grupoFacade.salvar(grupo);
    }
    
    public void editar(Grupo g){
        grupo = g;
    }
    
    public void excluir(Grupo g){
        grupoFacade.remover(g);
    }
    
    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
    
    public List<Grupo> getLista(){
        return grupoFacade.listaTodos();
    }   
}
